# W3C Color Lookup Tool #
***
This package provides an API for looking up hex, rgb, hsv, or hsl values of W3C-recognized colors. The Color class, when initialized with a valid identifier, will have as its attributes components of the various color model representations.

## Getting Started
***
These instructions will work for development and testing on a local machine and for deployment to a live system.

### Prerequisites
There are no dependencies for using the `colorlookup` package.

To (re)load the database from a csv file using the `loaddata.py` script, Pandas version 0.19.2 or greater is necessary. To install Pandas using pip:

```bash
$ pip install 'pandas>=0.19.2'
```

### Installing

#### Using pip

Install using pip from the command line:
```bash
$ pip install colorlookup
```

#### Manually

Create a temporary directory for the source files:

```bash
$ mkdir colorlookup && cd colorlookup
```
Clone the git repository:

```bash
$ git clone https://tpodlaski@bitbucket.org/tpodlaski/colorlookup.git
```

Run `setup.py`:

```bash
$ python setup.py install
```

### Sample Usage

```python3
>>> from colorlookup import Color
>>>
>>> brown = Color('brown')
>>>
>>> print(brown)
Color: Brown
  hex: #a52a2a
  rgb: (165, 42, 42)
>>>
>>> brown.hex
'#a52a2a'
>>> brown.rgb
(165, 42, 42)
>>> brown.r
165
>>> brown.g
42
>>> brown.b
42
>>> brown.rgb_f
(0.65, 0.16, 0.16)
>>> brown.r_f
0.65
>>> brown.g_f
0.16
>>> brown.b_f
0.6
>>> brown.hsl_sat
0.59
>>> brown.hsl_light
0.41
>>> brown.hsv_sat
0.75
>>> brown.hsv_val
0.65
```

## Versioning
***

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/tpodlaski/colorlookup/tags). 

## Authors
***

* **Tony Podlaski** - [http://www.neuraldump.com](http://www.neuraldump.com)

## License
***

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments
***
W3C Color Information can be found here:
[https://www.w3.org/TR/SVG/types.html#ColorKeywords](https://www.w3.org/TR/SVG/types.html#ColorKeywords)